#%%
import matplotlib.pyplot as plt
from scipy import signal
from scipy.io import wavfile

file_name = 'input/w11_record_engine_4.wav'

print(f"reading audio file: {file_name}")
sample_rate, samples = wavfile.read(file_name)

# samples = samples[:100000]

print(f"calculating spectogram...")
frequencies, times, spectrogram = signal.spectrogram(samples, sample_rate)


""" analyze wich column in the spectogram has all '0' """
import numpy as np

print("--------------------")
print("Error Analysis")
print("--------------------")
print("finding columns = 0...")

err_detected = False
err_col = []
for col_i in range(len(times)):
    col_val = spectrogram[:,col_i]
    if(np.all(col_val == 0)):
        err_detected = True
        err_col.append(col_i)
        # print(f'{col_i = }')

if(err_detected):
    print(f"> {len(err_col)} Errors detected in columns:")
    print(f"{err_col}")
else:
    print("> No Errors detected")

#%%
""" plot the spectogram """

# """it plots an specific sample idx"""
# idx_test = 1214
# window  = 20
# spectrogram_plot = spectrogram[:,idx_test-window: idx_test+window]
# times_plot = times[idx_test-window: idx_test+window]

"""it plots whole spectogram"""
spectrogram_plot = spectrogram
times_plot = times

def plot_spectogram(times, frequencies, spectrogram_plot):
    # ancho = 100
    # alto = 10
    # plt.figure(figsize=(ancho, alto))

    import matplotlib.colors as colors
    vmin = spectrogram_plot.min()
    if(vmin == 0): vmin = 1e-9

    plt.pcolormesh(times, frequencies, spectrogram_plot, 
                   norm=colors.LogNorm(vmin=vmin, vmax=spectrogram_plot.max()))
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    # plt.savefig(f"{file_name}.pdf")
    plt.show()

plot_spectogram(times_plot, frequencies, spectrogram_plot)


#%%
""" test how many zeros in a row are detected in the audio"""

# start from here so it does include the begining full of zeros
idx_from = 2700 
tmp_samples = samples[idx_from:]
zeros = (tmp_samples == 0)*1

cont = 0
thresh_n_zeros_to_save = 100
idx = 0
table = []
last_value_max = False
for i, sample_i in enumerate(zeros):
    if sample_i == 1:
        
        last_value_max = True
        cont += 1
                    
    else:
        if (last_value_max):
            if(cont >= thresh_n_zeros_to_save):
                table.append([i+idx_from, (i+idx_from)*(1/sample_rate), cont, cont*(1/sample_rate*1000)])
        last_value_max = False
        cont = 0

print(f'number of slots detected (slot >= {thresh_n_zeros_to_save} zeros in a row)= {len(table)}')

if (len(table)):
    def order_nparray_by_column(array, column, reverse=False):
        array = sorted(list(array), key=lambda a_entry: a_entry[column], reverse=reverse) 
        return np.array(array)

    table_order = order_nparray_by_column(np.array(table), 2, reverse=True)
    import pandas as pd
    table_order = pd.DataFrame(table_order, columns=['idx', 'idx_time(sec)', 'n_zeros', 'time slot (ms)'])

    import ipython_genutils
    display(table_order)


#%%
""" plot the slot """
idx_analyze = int(table_order.loc[0][0])
n_zeros = int(table_order.loc[0][2])

scale = 2
plt.plot(samples[idx_analyze - n_zeros*scale : idx_analyze + n_zeros*scale])

#%%
